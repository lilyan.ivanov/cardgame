package com.example.demo.entity;

import javax.persistence.*;

@Entity
@Table(name = "games")
public class Game {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private Double deposit;

    @OneToOne(cascade = CascadeType.ALL)
    private Deck deck;

    @OneToOne
    private Player player;

    @OneToOne
    private Card previousCard;

    public Game(){}

    public Game(Deck deck) {
        this.deck = deck;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Deck getDeck() {
        return deck;
    }

    public void setDeck(Deck deck) {
        this.deck = deck;
    }

    public boolean isEmpty() {
        return this.deck.isEmpty();
    }

    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    public Double getDeposit() {
        return deposit;
    }

    public void setDeposit(Double deposit) {
        this.deposit = deposit;
    }

    public Card getPreviousCard() {
        return previousCard;
    }

    public void setPreviousCard(Card card) {
        this.previousCard = card;
    }
}
