package com.example.demo.entity;

import com.example.demo.common.exception.EmptyDeckException;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "decks")
public class Deck {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @ManyToMany(cascade = {CascadeType.MERGE, CascadeType.PERSIST,CascadeType.REFRESH}, fetch = FetchType.EAGER)
    private List<Card> cards;

    public Deck() {
        this.cards = new ArrayList<>();
    }

    public void addCardToDeck(Card card) {
        this.cards.add(card);
    }

    public boolean isEmpty() {
        return this.cards.isEmpty();
    }

    public Card pop() throws EmptyDeckException {
        if (this.cards.size() >= 1) {
            var card = this.cards.get(cards.size() - 1);
            this.cards.remove(card);
            return card;
        }
        throw new EmptyDeckException("No more cards to deal");
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public List<Card> getCards() {
        return cards;
    }

    public void setCards(List<Card> cards) {
        this.cards = cards;
    }
}
