package com.example.demo.entity;

import com.example.demo.common.enums.CardRank;
import com.example.demo.common.enums.Suit;
import com.example.demo.payload.CardResponse;

import javax.persistence.*;

@Entity
@Table(name = "cards")
public class Card {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @Enumerated(value = EnumType.STRING)
    private Suit suit;
    @Enumerated(value = EnumType.STRING)
    private CardRank rank;
    public Card(){}

    public Card(Suit suit, CardRank rank) {
        this.suit = suit;
        this.rank = rank;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Suit getSuit() {
        return suit;
    }

    public void setSuit(Suit suit) {
        this.suit = suit;
    }

    public CardRank getRank() {
        return rank;
    }

    public void setRank(CardRank cardRank) {
        this.rank = cardRank;
    }

    public static CardResponse toCardResponse(Card card) {
        return new CardResponse(card.getSuit(), card.getRank());
    }

    public static Card toCard(CardResponse cardResponse) {
        return new Card(cardResponse.suit(), cardResponse.cardRank());
    }
}
