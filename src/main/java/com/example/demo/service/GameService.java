package com.example.demo.service;

import com.example.demo.entity.Player;
import com.example.demo.payload.CardResponse;
import com.example.demo.payload.GameRequest;

public interface GameService {
    CardResponse start(Player player, GameRequest request) throws Exception;
}
