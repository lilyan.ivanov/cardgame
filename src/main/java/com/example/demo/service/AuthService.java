package com.example.demo.service;

import com.example.demo.payload.LoginRequest;
import com.example.demo.payload.LoginResponse;

public interface AuthService {
    LoginResponse login(LoginRequest request) throws Exception;
}
