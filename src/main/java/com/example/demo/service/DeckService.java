package com.example.demo.service;

import com.example.demo.entity.Deck;
import com.example.demo.payload.CardResponse;

public interface DeckService {
    Deck populateDeck();

    CardResponse shuffleNewDeck(Long gameId, Long id) throws Exception;
}
