package com.example.demo.service.impl;

import com.example.demo.entity.Player;
import com.example.demo.payload.PlayerRequest;
import com.example.demo.repository.PlayerRepository;
import com.example.demo.service.PlayerService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class PlayerServiceImpl implements PlayerService {

    private final PasswordEncoder passwordEncoder;
    private final PlayerRepository playerRepository;

    public PlayerServiceImpl(PasswordEncoder passwordEncoder, PlayerRepository playerRepository) {
        this.passwordEncoder = passwordEncoder;
        this.playerRepository = playerRepository;
    }

    @Override
    @Transactional
    public void save(PlayerRequest request) throws Exception {
        this.throwIfEmailExists(request);
        var player = new Player();
        player.setPassword(passwordEncoder.encode(request.password()));
        player.setEmail(request.email());

        this.playerRepository.save(player);
    }

    private void throwIfEmailExists(PlayerRequest request) throws Exception {
        if (this.playerRepository.existsByEmail(request.email())) {
            throw new Exception();
        }
    }
}
