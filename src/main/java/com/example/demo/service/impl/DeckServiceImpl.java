package com.example.demo.service.impl;

import com.example.demo.entity.Card;
import com.example.demo.entity.Deck;
import com.example.demo.payload.CardResponse;
import com.example.demo.repository.DeckRepository;
import com.example.demo.repository.GameRepository;
import com.example.demo.service.CardService;
import com.example.demo.service.DeckService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;

@Service
public class DeckServiceImpl implements DeckService {

    private final DeckRepository deckRepository;
    private final GameRepository gameRepository;
    private final CardService cardService;

    public DeckServiceImpl(DeckRepository deckRepository, GameRepository gameRepository, CardService cardService) {
        this.deckRepository = deckRepository;
        this.gameRepository = gameRepository;
        this.cardService = cardService;
    }

    @Override
    public Deck populateDeck() {
        var deck = new Deck();
        for (long i = 1L; i <= 52L; i++) {
            deck.addCardToDeck(this.cardService.findById(i));
        }
        return this.shuffleDeck(deck);
    }

    @Override
    @Transactional
    public CardResponse shuffleNewDeck(Long gameId, Long id) throws Exception {
        var deck = new Deck();
        for (long i = 1L; i <= 52L; i++) {
            deck.addCardToDeck(this.cardService.findById(i));
        }
        this.shuffleDeck(deck);

        this.deckRepository.deleteById(id);
        var game = this.gameRepository.findById(gameId).orElseThrow();
        game.setDeck(deck);
        Card card = deck.pop();
        game.setPreviousCard(card);
        this.gameRepository.save(game);

        return Card.toCardResponse(card);
    }

    private Deck shuffleDeck(Deck deck) {
        Collections.shuffle(deck.getCards());
        return deck;
    }
}
