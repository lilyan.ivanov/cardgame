package com.example.demo.service.impl;

import com.example.demo.auth.JwtGenerator;
import com.example.demo.payload.LoginRequest;
import com.example.demo.payload.LoginResponse;
import com.example.demo.repository.PlayerRepository;
import com.example.demo.service.AuthService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class AuthServiceImpl implements AuthService {

    private final PlayerRepository playerRepository;
    private final JwtGenerator jwtGenerator;
    private final PasswordEncoder passwordEncoder;


    public AuthServiceImpl(PlayerRepository playerRepository, JwtGenerator jwtGenerator, PasswordEncoder passwordEncoder) {
        this.playerRepository = playerRepository;
        this.jwtGenerator = jwtGenerator;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public LoginResponse login(LoginRequest request) throws Exception {
        var user = this.playerRepository.findPlayerByEmail(request.email());
        this.throwIfIncorrectPassword(request.password(), user.getPassword());
        var token = this.jwtGenerator.generateToken(user);

        return new LoginResponse(token);
    }

    private void throwIfIncorrectPassword(String rawPassword, String encodedPassword) throws Exception {
        if (!this.passwordEncoder.matches(rawPassword, encodedPassword)) {
            throw new Exception();
        }
    }
}
