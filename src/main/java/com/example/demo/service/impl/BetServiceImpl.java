package com.example.demo.service.impl;

import com.example.demo.common.enums.BetRank;
import com.example.demo.common.enums.BetStatus;
import com.example.demo.common.exception.EmptyDeckException;
import com.example.demo.common.exception.InsufficientMoneyException;
import com.example.demo.entity.Card;
import com.example.demo.entity.Game;
import com.example.demo.entity.Player;
import com.example.demo.payload.BetRequest;
import com.example.demo.payload.BetResponse;
import com.example.demo.repository.GameRepository;
import com.example.demo.service.BetService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class BetServiceImpl implements BetService {

    private final GameRepository gameRepository;

    public BetServiceImpl(GameRepository gameRepository) {
        this.gameRepository = gameRepository;
    }

    @Override
    @Transactional
    public BetResponse bet(Player player, BetRequest request) throws InsufficientMoneyException, EmptyDeckException {
        var game = player.getCurrentGame();

        if (game.getDeposit() < request.bet()) {
            throw new InsufficientMoneyException("Not enough money exception");
        }

        var card = game.getDeck().pop();

        if (cardComparison(card, game, request) > 0) {
            return winBet(game, card, request);
        } else if (cardComparison(card, game, request) < 0) {
            return loseBet(game, card, request);
        }

        return refundWhenEqual(card);
    }

    private BetResponse winBet(Game game, Card card, BetRequest request) {
        game.setDeposit(game.getDeposit() + request.bet());
        game.setPreviousCard(card);
        this.gameRepository.save(game);
        return new BetResponse(Card.toCardResponse(card), BetStatus.WIN);
    }

    private BetResponse loseBet(Game game, Card card, BetRequest request) {
        game.setDeposit(game.getDeposit() - request.bet());
        game.setPreviousCard(card);
        this.gameRepository.save(game);
        return new BetResponse(Card.toCardResponse(card), BetStatus.LOST);
    }

    private BetResponse refundWhenEqual(Card card) {
        return new BetResponse(Card.toCardResponse(card), BetStatus.REFUNDED);
    }

    private int cardComparison(Card card, Game game, BetRequest request) {
        if (request.betRank().equals(BetRank.HIGHER)) {
            if (card.getRank().compareTo(game.getPreviousCard().getRank()) > 0) return 1;
            else if (card.getRank().compareTo(game.getPreviousCard().getRank()) < 0) return -1;
        }

        if (request.betRank().equals(BetRank.LOWER)) {
            if (card.getRank().compareTo(game.getPreviousCard().getRank()) > 0) return -1;
            else if (card.getRank().compareTo(game.getPreviousCard().getRank()) < 0) return 1;
        }

        return card.getRank().compareTo(game.getPreviousCard().getRank());
    }
}
