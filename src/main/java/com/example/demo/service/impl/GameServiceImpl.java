package com.example.demo.service.impl;

import com.example.demo.entity.Card;
import com.example.demo.entity.Deck;
import com.example.demo.entity.Game;
import com.example.demo.entity.Player;
import com.example.demo.payload.CardResponse;
import com.example.demo.payload.GameRequest;
import com.example.demo.repository.GameRepository;
import com.example.demo.repository.PlayerRepository;
import com.example.demo.service.DeckService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class GameServiceImpl implements com.example.demo.service.GameService {

    private final PlayerRepository playerRepository;
    private final DeckService deckService;
    private final GameRepository gameRepository;

    public GameServiceImpl(PlayerRepository playerRepository, DeckService deckService, GameRepository gameRepository) {
        this.playerRepository = playerRepository;
        this.deckService = deckService;
        this.gameRepository = gameRepository;
    }

    @Override
    @Transactional
    public CardResponse start(Player player, GameRequest request) throws Exception {

        if (player.getBalance() < request.deposit()) {
            throw new Exception();
        }
        if (player.isNewGame()) {
            return startNewGame(player, request);
        }

        return continueGame(player);
    }


    private CardResponse startNewGame(Player player, GameRequest request) throws Exception {
        var currentGame = new Game();
        var deck = this.deckService.populateDeck();

        currentGame.setDeck(deck);
        player.setBalance(player.getBalance() - request.deposit());
        currentGame.setDeposit(request.deposit());
        player.setCurrentGame(currentGame);

        var card = deck.pop();
        currentGame.setPreviousCard(card);
        currentGame.setPlayer(player);

        this.gameRepository.save(currentGame);
        this.playerRepository.save(player);

        return Card.toCardResponse(card);
    }

    private CardResponse continueGame(Player player) throws Exception {
        Game currentGame = player.getCurrentGame();
        Deck deck = currentGame.getDeck();

        return Card.toCardResponse(deck.pop());
    }
}
