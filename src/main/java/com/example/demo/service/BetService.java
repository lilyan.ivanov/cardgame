package com.example.demo.service;

import com.example.demo.common.exception.EmptyDeckException;
import com.example.demo.common.exception.InsufficientMoneyException;
import com.example.demo.entity.Player;
import com.example.demo.payload.BetRequest;
import com.example.demo.payload.BetResponse;

public interface BetService {
    BetResponse bet(Player player, BetRequest request) throws InsufficientMoneyException, EmptyDeckException;
}
