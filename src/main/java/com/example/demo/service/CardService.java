package com.example.demo.service;

import com.example.demo.entity.Card;

public interface CardService {
    Card findById(Long id);
}
