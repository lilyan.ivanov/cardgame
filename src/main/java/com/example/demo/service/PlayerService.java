package com.example.demo.service;

import com.example.demo.payload.PlayerRequest;

public interface PlayerService {
    void save(PlayerRequest request) throws Exception;

}
