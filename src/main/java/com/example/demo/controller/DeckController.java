package com.example.demo.controller;

import com.example.demo.payload.CardResponse;
import com.example.demo.service.DeckService;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/game/{gameId}/deck/{deckId}/shuffle")
public class DeckController {

    private final DeckService deckService;

    public DeckController(DeckService deckService) {
        this.deckService = deckService;
    }

    @PostMapping
    public CardResponse shuffle(@PathVariable Long gameId, @PathVariable Long deckId) throws Exception {
        return this.deckService.shuffleNewDeck(gameId, deckId);
    }
}
