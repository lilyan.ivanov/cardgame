package com.example.demo.controller;

import com.example.demo.payload.PlayerRequest;
import com.example.demo.service.PlayerService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/players")
public class PlayerController {

    private final PlayerService playerService;

    public PlayerController(PlayerService playerService) {
        this.playerService = playerService;
    }

    @PostMapping
    public void register(@RequestBody PlayerRequest request) throws Exception {
        this.playerService.save(request);
    }
}
