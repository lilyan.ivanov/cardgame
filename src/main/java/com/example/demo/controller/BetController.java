package com.example.demo.controller;

import com.example.demo.entity.Player;
import com.example.demo.payload.BetRequest;
import com.example.demo.payload.BetResponse;
import com.example.demo.service.BetService;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/bet")
public class BetController {

    private final BetService betService;

    public BetController(BetService betService) {
        this.betService = betService;
    }

    @PostMapping
    public BetResponse bet(@AuthenticationPrincipal Player player, @RequestBody BetRequest request) throws Exception {
        return this.betService.bet(player, request);
    }
}
