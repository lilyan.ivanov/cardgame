package com.example.demo.controller;

import com.example.demo.entity.Player;
import com.example.demo.payload.CardResponse;
import com.example.demo.payload.GameRequest;
import com.example.demo.service.GameService;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/game")
public class GameController {

    private final GameService gameService;

    public GameController(GameService gameService) {
        this.gameService = gameService;
    }

    @PostMapping
    public CardResponse start(@AuthenticationPrincipal Player player, @RequestBody GameRequest request) throws Exception {
        return this.gameService.start(player, request);
    }
}
