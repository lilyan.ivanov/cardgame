package com.example.demo.payload;

public record PlayerRequest(String email, String password) {
}
