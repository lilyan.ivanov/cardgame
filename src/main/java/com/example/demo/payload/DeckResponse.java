package com.example.demo.payload;

import java.util.List;

public record DeckResponse(Long id, List<CardResponse> deck) {
}
