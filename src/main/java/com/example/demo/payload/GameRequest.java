package com.example.demo.payload;

public record GameRequest(Double deposit) {
}
