package com.example.demo.payload;

public record LoginResponse(String token) {
}
