package com.example.demo.payload;

import com.example.demo.common.enums.CardRank;
import com.example.demo.common.enums.Suit;

public record CardResponse(Suit suit, CardRank cardRank) {
}
