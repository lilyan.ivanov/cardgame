package com.example.demo.payload;

public record GameResponse(Long id, DeckResponse deck) {
}
