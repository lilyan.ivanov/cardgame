package com.example.demo.payload;

import com.example.demo.common.enums.BetStatus;

public record BetResponse(CardResponse card, BetStatus status) {
}
