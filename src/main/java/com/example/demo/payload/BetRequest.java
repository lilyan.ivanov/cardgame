package com.example.demo.payload;

import com.example.demo.common.enums.BetRank;

public record BetRequest(Double bet, BetRank betRank) {
}
