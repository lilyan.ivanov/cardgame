package com.example.demo.payload;

public record LoginRequest(String email, String password) {
}
