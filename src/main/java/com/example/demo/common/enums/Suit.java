package com.example.demo.common.enums;

public enum Suit {
    SPADES,
    HEARTS,
    DIAMONDS,
    CLUBS
}
