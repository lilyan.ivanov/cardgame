package com.example.demo.common.enums;

public enum BetStatus {
    WIN, LOST, REFUNDED
}
