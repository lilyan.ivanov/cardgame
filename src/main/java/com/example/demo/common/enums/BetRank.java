package com.example.demo.common.enums;

public enum BetRank {
    HIGHER, LOWER, SHUFFLE
}
