package com.example.demo.common.exception;

public class EmptyDeckException extends Exception {

    public EmptyDeckException(String message) {
        super(message);
    }
}
