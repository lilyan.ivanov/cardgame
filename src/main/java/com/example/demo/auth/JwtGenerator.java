package com.example.demo.auth;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.example.demo.entity.Player;
import org.springframework.context.annotation.Configuration;

import java.util.Date;
import java.util.concurrent.TimeUnit;

@Configuration
public class JwtGenerator {

    public String generateToken(Player player) {
        return JWT.create()
                .withSubject(player.getEmail())
                .withExpiresAt(
                        new Date(
                                System.currentTimeMillis()
                                        + TimeUnit.DAYS.toMillis(2L)
                        )
                )
                .sign(Algorithm.HMAC512("verySecretKey"));
    }

    public DecodedJWT decodeToken(String token) {
        var pureToken = token.replace("Bearer ", "");

        return JWT.require(Algorithm.HMAC512("verySecretKey"))
                .build()
                .verify(pureToken);
    }
}
