package com.example.demo.auth.filter;


import com.example.demo.auth.JwtGenerator;
import com.example.demo.repository.PlayerRepository;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;
import org.springframework.stereotype.Component;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;

@Component
public class JWTAuthorizationFilter extends BasicAuthenticationFilter {

    private final JwtGenerator jwtGenerator;

    private final PlayerRepository playerRepository;

    public JWTAuthorizationFilter(
            AuthenticationManager authenticationManager,
            JwtGenerator jwtGenerator,
            PlayerRepository playerRepository
    ) {
        super(authenticationManager);
        this.jwtGenerator = jwtGenerator;
        this.playerRepository = playerRepository;
    }

    @Override
    protected void doFilterInternal(
            HttpServletRequest request,
            HttpServletResponse response,
            FilterChain chain
    ) throws IOException, ServletException {
        var token = request.getHeader("Authorization");

        if (token == null) {
            chain.doFilter(request, response);

            return;
        }

        var authentication = this.getAuthentication(token);

        SecurityContextHolder.getContext().setAuthentication(authentication);

        chain.doFilter(request, response);
    }

    public UsernamePasswordAuthenticationToken getAuthentication(String token) {
        var decodedToken = this.jwtGenerator.decodeToken(token);

        var player = this.playerRepository.findPlayerByEmail(decodedToken.getSubject());

        return new UsernamePasswordAuthenticationToken(
                player,
                null,
                new ArrayList<>()
        );
    }

}