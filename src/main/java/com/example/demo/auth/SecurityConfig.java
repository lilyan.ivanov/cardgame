package com.example.demo.auth;

import com.example.demo.auth.filter.JWTAuthorizationFilter;
import com.example.demo.repository.PlayerRepository;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@Configuration
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    private final JwtGenerator jwtGenerator;

    private final PlayerRepository playerRepository;

    public SecurityConfig(
            JwtGenerator jwtGenerator,
            PlayerRepository playerRepository

    ) {
        this.jwtGenerator = jwtGenerator;
        this.playerRepository = playerRepository;
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.cors().and().csrf().disable()
                .authorizeRequests().anyRequest().permitAll()
                .and()
                .addFilter(
                        new JWTAuthorizationFilter(
                                this.authenticationManagerBean(),
                                this.jwtGenerator,
                                this.playerRepository
                        )
                );
    }

    @Override
    @Bean
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

}
