CREATE TABLE public.cards
(
    id   BIGSERIAL
        CONSTRAINT card_id_primary PRIMARY KEY,
    rank VARCHAR (255) NOT NULL,
    suit VARCHAR (255) NOT NULL
);
